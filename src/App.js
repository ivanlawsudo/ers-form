import React, { useState } from "react";
import axios from "axios";
import randomstring from "randomstring";

const App = () => {
  const [name, setName] = useState("");
  const [key, setKey] = useState([]);

  function handleChange(event) {
    setName(event.target.value);
  }

  function handleSubmit(event) {
    event.preventDefault();

    axios
      .post(`https://api.partyrental.marketing/v1/admin/installs`, {
        name: name,
        token: name + "_" + randomstring.generate(32)
      })
      .then(res => {
        axios
          .get(`https://api.partyrental.marketing/v1/admin/installs`)
          .then(res => {
            setKey(res.data.data);
          });
      });
  }

  console.log(key);

  return (
    <div style={{ textAlign: "center", marginTop: "5vh" }}>
      <label>Add New</label>
      <form
        className="form-inline justify-content-center"
        onSubmit={handleSubmit}
      >
        <div className="form-group">
          <input
            type="text"
            className="form-control"
            placeholder="Enter ID"
            onChange={handleChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Add
        </button>
      </form>

      <label style={{ marginTop: "10vh" }}>ERS Api Keys</label>
      <div class="card" style={{ width: 650, maxWidth: "95%", margin: "auto" }}>
        <div class="card-body">
          {key.map((id, key) => (
            <div key={key}>
              <label>id: {id._id}</label>
              <br />
              <label>name: {id.name}</label>
              <br />
              <label>token: {id.token}</label>
              <hr />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default App;
